const express=require('express')
const path=require('path')
const exphbs=require('express-handlebars')
const homeRouter =require('./routers/home')
const coursesRouter= require('./routers/courses')
const cardRouter= require('./routers/card')
const addRouter = require('./routers/add')

const app=express()

const hbs=exphbs.create({      //conect plagin exxpress-handlebars
    defaultLayout:'main',
    extname:'hbs'
});
app.engine('hbs',hbs.engine)    //connect engens exxpress-handlebars
app.set('view engine','hbs')
app.set('views','views')

app.use(express.static(path.join(__dirname,'public')))
app.use(express.urlencoded({extended:true}))    //decoding data from response
app.use('/',homeRouter)                                //connect  router
app.use('/courses',coursesRouter)                        //connect  router
app.use('/add',addRouter)                              //connect  router
app.use('/card',cardRouter)



const PORT =process.env.PORT || 3000                   //start localhost
app.listen(PORT,()=>{
    console.log(`Server is running on port ${PORT} `)
});
